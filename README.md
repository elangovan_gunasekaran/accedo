# accedo

Interview application developed using [Sails](http://sailsjs.org)

Install sails globally

###How to run in development mode?
1) npm install
2) bower install
3) sails lift

You can access the server in localhost:1337

To run test cases run
    *grunt test*

###How to run in production mode?
1) npm install
2) bower install
3) sails lift --prod

To Run in production (AWS server) :

Use pm2 