/**
 * Application Main File
 *
 * @module         AccedoApp
 * @description    Main angular application file.
 * @author         Elangovan G <govansmailbox@gmail.com>
 */

angular.module('AccedoApp', ['ui.bootstrap', 'toaster', 'ngAnimate', 'cfp.hotkeys', 'ui.router', 'ngAside'])
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /list
    $urlRouterProvider.otherwise("/list");

    // Now set up the states
    $stateProvider
        .state('list', {
            url: '/list',
            templateUrl: 'templates/list.html'
        })
        .state('player', {
            url: '/player',
            templateUrl: 'templates/player.html',
            params: {
                movie: null
            }
        });
}]);
