/**
 * PlayerCtrl
 *
 * @module         AccedoApp
 * @description    Controller for handling video player.
 * @author         Elangovan G <govansmailbox@gmail.com>
 */

angular.module('AccedoApp')
.controller('PlayerCtrl', ['$scope', '$rootScope', '$stateParams', '$sce', '$state', '$http', 'toaster',
        function($scope, $rootScope, $stateParams, $sce, $state, $http, toaster) {
    if ($stateParams.movie) {
        $scope.movie = $stateParams.movie;
        var url, videoId;

        if ( $scope.movie.contents && $scope.movie.contents.length) {
            url = $scope.movie.contents[0].url;
            videoId = $scope.movie.id;
        } else {
            url = $scope.movie.videoUrl;
            videoId = $scope.movie.videoId;
        }

        $scope.url = $sce.trustAsResourceUrl(url);

        $http.post('/history/create', {
            title: $scope.movie.title,
            videoId: videoId,
            videoUrl: url,
            token: $rootScope.token
        }).then(function(res) {

        }, function(err) {
            toaster.pop('error', 'Oops!', err.error);
        });
    } else {
        $state.go('list');
    }
}]);
