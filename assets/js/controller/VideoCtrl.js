/**
 * VideoCtrl
 *
 * @module         AccedoApp
 * @description    Controller for handling videos.
 * @author         Elangovan G <govansmailbox@gmail.com>
 */

angular.module('AccedoApp')
.controller('VideoCtrl', ['$scope', '$rootScope', '$http', 'toaster', 'hotkeys', '$state', '$aside', '$window',
        function($scope, $rootScope, $http, toaster, hotkeys, $state, $aside, $window) {
    $scope.myInterval = false;
    $scope.noWrapSlides = true;
    $scope.active = $rootScope.selectedMovieIndex ? Math.floor($rootScope.selectedMovieIndex / 4) : 0;

    $http.get('https://demo2697834.mockable.io/movies').then(function(res) {
        $scope.movies = res.data.entries;
        $scope.groupedMovies = [];
        res.data.entries.forEach(function(data, index) {
            var groupIndex = Math.floor(index / 4);

            if (undefined === $scope.groupedMovies[groupIndex]) {
                $scope.groupedMovies.push({movies: []});
            }

            data.index = index;
            $scope.groupedMovies[groupIndex].movies.push(data);
        });
    }, function(err) {
        toaster.pop('error', 'Oops!', 'Something went wrong');
    });

    if ($window.localStorage.token) {
        $rootScope.token = $window.localStorage.token;
    } else {
        $http.post('/user/create', {
            id: generateUUID()
        }).then(function(res) {
            $window.localStorage.token = res.data.token;
            $rootScope.token = res.data.token;
        }, function(err) {
            toaster.pop('error', 'Oops!', err.error);
        });
    }

    function generateUUID() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    }

    function navigateRight() {
        if (undefined === $rootScope.selectedMovieIndex) {
            $rootScope.selectedMovieIndex = 0;
        } else if ($rootScope.selectedMovieIndex < $scope.movies.length) {
            $rootScope.selectedMovieIndex++;
            $scope.active = Math.floor($rootScope.selectedMovieIndex / 4);
        } else {
            $rootScope.selectedMovieIndex = $scope.movies.length - 1;
        }
    }

    function navigateLeft() {
        if ($rootScope.selectedMovieIndex > 0) {
            $rootScope.selectedMovieIndex--;
            $scope.active = Math.floor($rootScope.selectedMovieIndex / 4);
        } else {
            $rootScope.selectedMovieIndex = 0;
        }
    }

    function playVideo() {
        $state.go('player', {movie: $scope.movies[$rootScope.selectedMovieIndex]});
    }

    hotkeys.bindTo($scope)
        .add({
            combo: 'right',
            description: 'Navigate Right',
            callback: navigateRight
        }).add({
            combo: 'left',
            description: 'Navigate Left',
            callback: navigateLeft
        }).add({
            combo: 'enter',
            description: 'Play Video',
            callback: playVideo
        });

    $scope.getNextIndex = function(index) {
        if (index - $scope.movies.length >= 0) {
            return index - $scope.movies.length;
        } else {
            return index;
        }
    };

    $rootScope.openHistoryModal = function() {
        $aside.open({
            templateUrl: 'templates/history.html',
            controller: 'HistoryCtrl',
            placement: 'right',
            size: 'md'
        });
    };
}]);
