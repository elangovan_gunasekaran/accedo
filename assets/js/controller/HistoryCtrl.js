/**
 * HistoryCtrl
 *
 * @module         AccedoApp
 * @description    Controller for handling video history.
 * @author         Elangovan G <govansmailbox@gmail.com>
 */

angular.module('AccedoApp')
.controller('HistoryCtrl', ['$scope', '$rootScope', '$http', 'toaster', '$aside', '$uibModalInstance',
        function($scope, $rootScope, $http, toaster, $aside, $uibModalInstance) {

    if ($rootScope.token) {
        $http.get('/history/user', {
            params: {
                token: $rootScope.token
            }
        }).then(function(res) {
            $scope.history = res.data;
        }, function(err) {
            $scope.history = [];
        });
    }

    $scope.closeModal = function() {
        $uibModalInstance.close();
    };

    $scope.clearHistory = function() {
        $http.delete('/history/clear', {
            data: {
                token: $rootScope.token
            }
        }).then(function(res) {
            $scope.history = [];
        }, function(err) {
            toaster.pop('error', 'Oops!', 'Cannot clear history');
        });
    };
}]);
