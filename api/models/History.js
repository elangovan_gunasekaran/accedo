/**
 * History.js
 *
 * @module  		models/histories
 * @description 	Represents a single history in the database.
 * @author 			Elangovan G <govansmailbox@gmail.com>
 */

module.exports = {

    autoCreatedAt: true,
    attributes: {
        title: {
            type: 'string'
        },
        videoId: {
            type: 'string',
            required: true
        },
        videoUrl: {
            type: 'string',
            required: true
        },
        user: {
            model: 'user',
            required: true
        }
    }

};
