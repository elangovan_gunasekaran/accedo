/**
 * User.js
 *
 * @module          models/users
 * @description     Represents a single user in the database.
 * @author          Elangovan G <govansmailbox@gmail.com>
 */

module.exports = {

    attributes: {
        id: {
            type: 'string',
            required: true,
            primaryKey: true
        }
    }
};
