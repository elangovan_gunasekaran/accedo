/**
 * UserController
 *
 * @module         controllers/users
 * @description    All user related tasks.
 * @author         Elangovan G <govansmailbox@gmail.com>
 */

module.exports = {
    /**
     * @description     Create a new user.
     * @author          Elangovan G <govansmailbox@gmail.com>
	 *
	 * @api             {post} /user/create Create
	 * @apiName         create
	 * @apiGroup        User
	 *
	 * @apiParam        {String} id The user's id.
     *
     * @apiSuccess      {Object} user Returns the new record along with a JSON Web Token.
     */
    create: function(req, res) {
        var data = {
            id: req.param('id')
        };

        User.create(data, function(err, user) {
            if (err) {
                return res.serverError({error: 'Could not create a user session. History cannot be saved.'});
            }

            var jwt = require('jsonwebtoken');
            var jwtPayload  = {
                server  : 'Accedo',
                userId  : user.id
            };
            user.token = jwt.sign(jwtPayload, 'u%v-c_p~F}*7j5eC');

            return res.created(user);
        });
    }
};
