/**
 * HistoryController
 *
 * @module         controllers/history
 * @description    All history related tasks.
 * @author         Elangovan G <govansmailbox@gmail.com>
 */

module.exports = {
    /**
     * @description     Create a new history.
     * @author          Elangovan G <govansmailbox@gmail.com>
     *
     * @api             {post} /history/create Create
     * @apiName         create
     * @apiGroup        History
     *
     * @apiParam        {String} title The movie title.
     * @apiParam        {String} videoId The movie id.
     * @apiParam        {String} videoUrl The movie url.
     * @apiParam		{String} x-access-token The access token retrieved during user creation.
     *
     * @apiSuccess      {Object} history Returns the new record.
     */
    create: function(req, res) {
        var data = {
            title: req.param('title'),
            videoId: req.param('videoId'),
            videoUrl: req.param('videoUrl'),
            user: req.decoded.userId
        };

        History.create(data, function(err, history) {
            if (err) {
                return res.serverError({error: 'Cannot Save History'});
            }

            return res.created(history);
        });
    },

    /**
     * @description     Find history of a user.
     * @author          Elangovan G <govansmailbox@gmail.com>
     *
     * @api             {get} /history/user findByUserId
     * @apiName         findByUserId
     * @apiGroup        History
     *
     * @apiParam        {String} x-access-token The access token retrieved during user creation.
     *
     * @apiSuccess      {Object} history Returns the history for the given user.
     */
    findByUserId: function(req, res) {
        History.find({user: req.decoded.userId}).sort('createdAt DESC').exec(function(err, history) {
            if (err) {
                return res.serverError();
            }

            return res.ok(history);
        });
    },

    /**
     * @description     Clear history of a user.
     * @author          Elangovan G <govansmailbox@gmail.com>
     *
     * @api             {delete} /history/clear clearHistory
     * @apiName         clearHistory
     * @apiGroup        History
     *
     * @apiParam        {String} x-access-token The access token retrieved during user creation.
     *
     * @apiSuccess      {String} success Returns the succes string.
     */
    clearHistory: function(req, res) {
        History.destroy({user: req.decoded.userId}, function(err, history) {
            if (err) {
                return res.serverError();
            }

            return res.ok({success: 'History Cleared'});
        });
    },

    /**
     * @description     Delete sigle history of a user.
     * @author          Elangovan G <govansmailbox@gmail.com>
     *
     * @api             {delete} /history/:id delete
     * @apiName         delete
     * @apiGroup        History
     *
     * @apiParam        {String} id The object id of the history.
     * @apiParam        {String} x-access-token The access token retrieved during user creation.
     *
     * @apiSuccess      {String} success Returns the succes string.
     */
    delete: function(req, res) {
        History.find({id: req.param('id')}, function(err, history) {
            if (err) {
                return res.serverError();
            }

            if (history.length < 1) {
                return res.notFound({error: 'History Not Found'});
            } else if (history[0].user === req.decoded.userId) {
                History.destroy({user: req.decoded.userId}, function(err, history) {
                    if (err) {
                        return res.serverError();
                    }

                    return res.ok({success: 'History deleted'});
                });
            } else {
                return res.forbidden({error: 'Access Denied'});
            }
        });
    }

};
