/**
 * verifyToken
 *
 * @module         policies/verifytoken
 * @description    Allows access to a route only if the JWT is valid.
 * @author         Elangovan G <govansmailbox@gmail.com>
 */
module.exports = function(req, res, next) {

    var token = req.param('token') || req.headers['x-access-token'];

    if (token) {
        var jwt = require('jsonwebtoken');

        jwt.verify(token, 'u%v-c_p~F}*7j5eC', function(err, decoded) {

			if (err) return res.forbidden({ error: 'You provided an invalid token.' });
            // Save the token to be used in the controller
            req.decoded = decoded;

            // Check if the user exists
            if (decoded.userId) {
                User.find({ id: decoded.userId }).exec(function(err, user) {
                    if (err) res.serverError({ error: 'Database error' });

                    if (user.length > 0) {
                        return next();
                    } else {
                        return res.notFound({ error: 'Could not find the user' });
                    }
                });
            }
        });
    } else {
        return res.forbidden({ error: 'You did not provide a token.' });
    }

};
