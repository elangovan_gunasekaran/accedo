var sails = require('sails');

var testConfig = {
    environment: 'test',
    port: 1337,
    connections: {
        testDB: {
            adapter: 'sails-memory'
        }
    },
    connection: 'testDB',
    models: {
        migrate: 'drop'
    }
};

before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift.
  this.timeout(5000);

  sails.lift(testConfig, function(err, server) {
    if (err) return done(err);
    done(err, sails);
  });
});

after(function(done) {
  sails.lower(done);
});
