var request = require('supertest');

describe('HistoryController', function() {

    var createdUser;

    describe('#create()', function() {

        before(function(done) {
            var user = {
                id: 'ccc'
            };

            request(sails.hooks.http.app)
                .post('/user/create')
                .send(user)
                .expect(201)
                .end(function(err, res) {
                    if (err) return done(err);

                    createdUser = res.body;
                    done();
                });
        });

        it('should create history', function(done) {

            var data = {
                title: '11111111',
                videoId: '1111111',
                videoUrl: '111111',
                token: createdUser.token
            };

            request(sails.hooks.http.app)
                .post('/history/create')
                .send(data)
                .expect(201, done);
        });
    });

    describe('#findByUserId()', function() {

        it('should fetch history of users', function(done) {
            var data = {
                token: createdUser.token
            };

            request(sails.hooks.http.app)
                .get('/history/user')
                .query(data)
                .expect(200)
                .end(function(err, res) {
                    if (err) return done(err);

                    if (res.body.length < 1) return done('No records found');

                    done();
                });
        });

    });

    describe('#clearHistory', function() {

        it('should clear all history of a user', function(done) {
            var data = {
                token: createdUser.token
            };

            request(sails.hooks.http.app)
                .delete('/history/clear')
                .send(data)
                .expect(200)
                .end(function(err, res) {
                    if (err) return done(err);

                    if (!res.body.success) return done('History not cleared');

                    done();
                });
        });

    });

    describe('#delete', function() {
        var createdHistory;
        var wrongUser;

        before(function(done) {
            var data = {
                title: '11111111',
                videoId: '1111111',
                videoUrl: '111111',
                token: createdUser.token
            };

            request(sails.hooks.http.app)
                .post('/history/create')
                .send(data)
                .expect(201)
                .end(function(err, res) {
                    if (err) return done(err);

                    createdHistory = res.body;

                    var user = {
                        id: 'ddd'
                    };

                    request(sails.hooks.http.app)
                        .post('/user/create')
                        .send(user)
                        .expect(201)
                        .end(function(err, res) {
                            if (err) return done(err);

                            wrongUser = res.body;
                            done();
                        });

                });

        });

        it('should throw not found error', function(done) {
            var data = {
                token: createdUser.token
            };

            request(sails.hooks.http.app)
                .delete('/history/okok')
                .send(data)
                .expect(404, done);
        });

        it('should throw forbidden error', function(done) {
            var data = {
                token: wrongUser.token
            };

            request(sails.hooks.http.app)
                .delete('/history/' + createdHistory.id)
                .send(data)
                .expect(403, done);
        });

        it('should delete a single entry of history for a user', function(done) {
            var data = {
                token: createdUser.token
            };

            request(sails.hooks.http.app)
                .delete('/history/' + createdHistory.id)
                .send(data)
                .expect(200)
                .end(function(err, res) {
                    if (err) return done(err);

                    if (!res.body.success) return done('History deleted');

                    done();
                });
        });

    });
});
