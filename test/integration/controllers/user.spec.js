var request = require('supertest');

describe('UserController', function() {

    describe('#create()', function() {
        it('should create user', function(done) {

            var data = {
                id : '222222'
            };

            request(sails.hooks.http.app)
                .post('/user/create')
                .send(data)
                .expect(201, done);
        });
    });
});
