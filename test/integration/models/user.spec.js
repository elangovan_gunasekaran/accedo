var assert = require('chai').assert;

describe('UserModel', function() {

    describe('#create()', function() {
        it('should check create function', function (done) {
            var user = {
                id: '11111111'
            };

            User.create(user)
            .then(function(results) {
                done();
            })
            .catch(done);
        });
    });

    describe('#find()', function() {
        it('should check find function', function (done) {
            User.find()
            .then(function(results) {
                assert.isAtLeast(results.length, 1, 'Atleast 1 record found');
                done();
            })
            .catch(done);
        });
    });

});
