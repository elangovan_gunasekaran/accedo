var assert = require('chai').assert;

describe('HistoryModel', function() {
    var createdUser;

    describe('#create()', function() {

        before(function(done) {
            var user = {
                id: 'aaa'
            };

            User.create(user)
            .then(function(result) {
                createdUser = result;
                done();
            })
            .catch(done);
        });

        it('should check create function', function (done) {
            var history = {
                title: '11111111',
                videoId: '1111111',
                videoUrl: '111111',
                user: createdUser.id
            };

            History.create(history)
            .then(function(results) {
                done();
            })
            .catch(done);
        });
    });

    describe('#find()', function() {
        it('should check find function', function (done) {
            History.find()
            .then(function(results) {
                assert.isAtLeast(results.length, 1, 'Atleast 1 record found');
                assert.isOk(results[0].createdAt, 'Record has createAt time');
                done();
            })
            .catch(done);
        });
    });

    describe('#findByUserId()', function() {
        it('should check find function by given user id', function (done) {
            History.find({user: createdUser.id})
            .then(function(results) {
                assert.isAtLeast(results.length, 1, 'Atleast 1 record found');
                assert.isOk(results[0].user == createdUser.id, 'Record has created user');
                done();
            })
            .catch(done);
        });
    });

    describe('#deleteByUserId()', function() {
        it('should check delete by given user id', function (done) {
            History.destroy({user: createdUser.id})
            .then(function(results) {
                assert.isAtLeast(results.length, 1, 'Atleast 1 record deleted');
                assert.isOk(results[0].user == createdUser.id, 'Record has been deleted by created user');
                done();
            })
            .catch(done);
        });
    });

});
