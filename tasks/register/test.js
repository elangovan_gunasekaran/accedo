/**
 * `test`
 *
 * ---------------------------------------------------------------
 *
 * This Grunt tasklist will be executed if you run
 * `grunt test` in a development environment.
 */
module.exports = function(grunt) {
  grunt.registerTask('test', [
    'mochaTest'
  ]);
};
